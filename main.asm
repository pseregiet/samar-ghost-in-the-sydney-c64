// Ghost in the Sydney by Samar Productions 2019
// Code  by   Golara
// Gfx   by   Isildur
// Music by   Jammer

.const yscroll = 3
.const irq1_line = 5
.const irq2_line = 246
.const drive_scrolltext_line = 120
.const logos_irq_line = 65
.const d020c = $02
.const d021c = $0a
.label real_colorram = $d800

* = $10 "ZP STUFF" virtual
scrollptr:    .fill 02, $00
scrollbuffer: .fill 21, $00 
scrollrol:    .fill 16, $00
zptmp1:       .fill 01, $00
zptmp2:       .fill 01, $00
zptmp3:       .fill 01, $00

.var muzak = LoadSid("AnimeGirl_finalzp.sid")

:BasicUpstart2(entry)
entry:
        sei
        lda #$35
        sta $01
        lda #$7f
        sta $dc0d
        sta $dd0d
        lda $dc0d
        lda $dd0d

//fill BASIC screen with spaces
        lda #$20
        ldx #$00
!:      sta $400,x
        sta $500,x
        sta $600,x
        sta $700,x
        inx
        bne !-

        lda #d020c
        sta $d020
        sta $d021

        ldx #$ff
        txs

        lda #$00
        sta $7fff

        lda #$00
        sta $d025
        lda #$0b
        sta $d026

        lda #$00
        jsr muzak.init

//fill up scroller buffer with spaces
        lda #$20
        ldx #$70
!:
        sta scrollptr,x
        dex
        bpl !-

        

        lda #<text
        sta scrollptr+0
        lda #>text
        sta scrollptr+1

//fill all of color ram with $a (color of the background)
        ldx #$00
        lda #$aa
!:
        .for (var i = 0; i < 4; i++)
        {
            sta $d800   + $100*i, x
        }
        inx
        bne !-

        lda #<introirq
        sta $fffe
        lda #>introirq
        sta $ffff
        lda #249
        sta $d012


!:      lda $d011
        and #$80
        bne !-
        lda #10
!:      cmp $d012
        bne !-

        lda #$02
        sta $dd00
        lda #$80
        sta $d018
        lda #$18
        sta $d016

        lda #$1b
        sta $d011

        lda #$01
        sta $d01a
        sta $d019
        cli
// color fade is run from outside irq.
// irq changes start_fade to break the endless loop
// when fade is done start_fade is restored to zero
// and code keeps on waiting for another fade call
endloop:
        lda start_fade:#$00
        beq endloop
!:
        jsr do_fade_in
        lda fade_data+999
        bne !-
        inc start_fade
!:
        jmp !-

//this intro does the sinus color drop
introirq:
{
        sta rega
        stx regx
        sty regy

        lda #$03
        sta $d011
        ldx #d020c
        lda #$fb
!:      cmp $d012
        bne !-
        stx $d021

        jsr muzak.play
!:      lda $d011
        and #$80
        bne !-
        ldx #d021c
        stx $d021

        ldy #d020c
        ldx sineidx:#$00
        lda intro_sintab,x
!:      cmp $d012
        bne !-

        sty $d021
        lda #$8+3
        sta $d011

        inc sineidx
        bne endirq

end_of_intro:
        lda #$38 + yscroll
        sta $d011
        
        lda #$02
        sta $dd00
        lda #$80
        sta $d018
        lda #$18
        sta $d016
        jmp irq2

        

endirq:
        lda rega:#$00
        ldx regx:#$00
        ldy regy:#$00
        inc $d019
        rti
}
// this irq moves the SAMAR / FLASHBACK sprite logos
logos_irq:
{
        sta rega
        stx regx
        sty regy

        lda start_fade
        cmp #3
        beq !+
        jmp endirq
!:
        lda #logos_irq_line + 5
        sta $d001
        sta $d003
        sta $d005
        sta $d007
        sta $d009

        lda #$1f
        sta $d015
        sta $d01c
        lda #$00
        sta $d01d
        sta $d017

        lda #$01
        sta $d026

        lda whichlogo:#00
        and #1
        bne logo2
        //set logo1(samar)
        ldx #samarlogo/$40
        stx screen + $3f8 + 0
        inx
        stx screen + $3f8 + 1
        inx
        stx screen + $3f8 + 2
        inx
        stx screen + $3f8 + 3
        inx
        stx screen + $3f8 + 4
        jmp setx
logo2:
        //set logo2(flashaback)
        ldx #flashbacklogo/$40
        stx screen + $3f8 + 0
        inx
        stx screen + $3f8 + 1
        inx
        stx screen + $3f8 + 2
        inx
        stx screen + $3f8 + 3
        inx
        stx screen + $3f8 + 4
setx:

        clc
        ldx posx:#$00
        lda logostab,x
        sta $d000
        adc #24
        clc
        sta $d002
        adc #24
        clc
        sta $d004
        adc #24
        clc
        sta $d006
        adc #24
        clc
        sta $d008
        lda logostabd010,x
        sta $d010

        inx
        stx posx
        bne !+
        inc whichlogo
!:
endirq:
        lda #<drive_scrolltext_irq
        sta $fffe
        lda #>drive_scrolltext_irq
        sta $ffff
        lda #drive_scrolltext_line
        sta $d012

        lda rega:#$00
        ldx regx:#$00
        ldy regy:#$00
        inc $d019
        rti
}

// irq at the top of the screen.
// sets up the head sprites.
.const sprite1x = 67+24
irq1:
{
        pha
        txa
        pha
        tya
        pha

        lda #d021c
        sta $d021

        lda #$00
        sta $d010
        lda #$38 + yscroll
        sta $d011
        lda #$ff
        sta $d015
        sta $d01c
        
        lda #$08
        sta $d001
        sta $d003
        sta $d005
        sta $d007
        lda #$08+21
        sta $d009
        sta $d00b
        sta $d00d
        sta $d00f

        lda #sprite1x+24*0
        sta $d000
        sta $d008
        lda #sprite1x+24*1
        sta $d002
        sta $d00a
        lda #sprite1x+24*2
        sta $d004
        sta $d00c
        lda #sprite1x+24*3
        sta $d006
        sta $d00e

        ldx #bitmapsprites/$40
        .for (var i = 0; i < 8; i++)
        {
            stx screen + $3f8 + i
            .if (i != 7){inx}
        }
        inc skipper
        lda start_fade
        bne !+

        lda skipper:#$00
        and #1
        bne !+

        ldx coloridx:#$05
        lda fadegen + $06*6,x
        sta col1
        lda fadegen + $00*6,x
        sta col2
        lda fadegen + $0b*6,x
        sta col3

        dex
        stx coloridx
        bpl !+
        inc start_fade
!:
        lda col1:#$0a
        .for (var i = 0; i < 8; i++)
        {
            sta $d027+i
        }
        lda col2:#$0a
        sta $d025
        lda col3:#$0a
        sta $d026

        lda #<logos_irq
        sta $fffe
        lda #>logos_irq
        sta $ffff
        lda #logos_irq_line
        sta $d012


        pla
        tay
        pla
        tax
        pla
        inc $d019
        rti
}
//bottom sprites, multiplexed
.const sprite2x = 32
irq2:
{
        pha
        txa
        pha
        tya
        pha

        lda start_fade
        cmp #2
        bne donotfade
        inc skipper
        lda skipper:#$00
        and #1
        bne donotfade
        ldx colidx:#$05
        lda fadegen2 + $06*6,x
        sta col1
        lda fadegen2 + $00*6,x
        sta col2
        lda fadegen2 + $0b*6,x
        sta col3
        lda fadegen2 + $03*6,x
        sta col4
        dex
        stx colidx
        bpl !+
        inc start_fade
!:
donotfade:

        lda #$00
        sta $d01d
        sta $d017
        sta $d01b
        lda #$ff
        sta $d015
        sta $d01c
        lda #$80
        sta $d010        

        lda #249
        sta $d001
        sta $d003
        sta $d005
        sta $d007
        sta $d009
        sta $d00b
        sta $d00d
        sta $d00f

        .for (var i = 0; i < 7; i++)
        {
            lda #sprite2x +24*i
            sta $d000 + i*2
        }

        lda #262
        sta $d00e

        ldx #(bitmapsprites/$40) + 8
        .for (var i = 0; i < 8; i++)
        {
            stx screen + $3f8 + i
            .if (i != 7){inx}
        }
        
        lda #$30 + yscroll
        sta $d011

        lda col1:#$02
        .for (var i = 0; i < 7; i++)
        {
            sta $d027+i
        }
        lda col2:#$02
        sta $d025
        lda col3:#$02
        sta $d026
        lda col4:#$02

        ldy #d020c
        sty $d021

        sta $d027+7
        

        

        lda #<irq1
        sta $fffe
        lda #>irq1
        sta $ffff
        lda #irq1_line
        sta $d012

        
        lda #249+21

        sta $d001
        sta $d003
        sta $d005
        sta $d007
        sta $d009
        sta $d00b
        sta $d00d

        sta $d00f

        tax
        dex

!:      cpx $d012
        bne !-

        ldy #(bitmapsprites/$40) + 8+8
        lda #(bitmapsprites/$40) + 8+9
        ldx #(bitmapsprites/$40) + 8+10        
        pha
        pla
        pha
        pla
        pha
        pla
        pha
        pla
        sty screen + $3f8
        sta screen + $3f9
        .for (var i = 2; i < 8; i++)
        {
            stx screen + $3f8 + i
            .if (i != 7){inx}
        }
        lda #282
        sta $d00e
        inc $d019
        cli

        lda start_fade
        cmp #3
        bne !+
        jsr copy_bitmap_animation
!:
        pla
        tay
        pla
        tax
        pla
        rti
}
//draw font on sprites
.const drawline = 5
draw_sprite1:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*00 + drawline*3, y
        }
        rts
draw_sprite2:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*01 + drawline*3, y
        }
        rts
draw_sprite3:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*02 + drawline*3, y
        }
        rts
draw_sprite4:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*03 + drawline*3, y
        }
        rts
draw_sprite5:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*04 + drawline*3, y
        }
        rts
draw_sprite6:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*05 + drawline*3, y
        }
        rts

draw_sprite7:
        .for (var i = 0; i < 16; i++)
        {
            lda charset + 64*i, x
            sta scrollsprites1 + i*3 + 64*06 + drawline*3, y
        }
        rts

.var draw_list = List().add(draw_sprite1,draw_sprite2,draw_sprite3
                            ,draw_sprite4,draw_sprite5,draw_sprite6
                            ,draw_sprite7)

move_scrolltext_buffer:
        ldx #$00
!:
        lda scrollbuffer + 1,x
        sta scrollbuffer + 0,x
        inx
        cpx #21
        bne !-
        ldy #$00
        lda (scrollptr),y
        bne !+
        lda #<text
        sta scrollptr+0
        lda #>text
        sta scrollptr+1
        lda #$20
!:
        sta scrollbuffer+21
        inc scrollptr
        bne !+
        inc scrollptr+1
!:
        rts

drive_scrolltext_irq:
{
        pha
        txa
        pha
        tya
        pha
// dont move scrolltext
// if all graphics are not faded in
        lda start_fade
        cmp #3
        beq !+
        jsr muzak.play
        jmp endirq
!:
        ldx #$1
        stx $d01c
        ldx #$ff
        stx $d015
        ldx #$02
        stx $d01b

        lda #$01
        .for (var i = 1; i < 8; i++)
        {
            sta $d027+i
        }

        ldx scrollpos:#$07
        dex
        bpl !+
        ldx #$07
!:
        lda #176
        sta $d000
        lda sprite1xtab,x
        sta $d002
        lda sprite2xtab,x
        sta $d004
        lda sprite3xtab,x
        sta $d006
        lda sprite4xtab,x
        sta $d008
        lda sprite5xtab,x
        sta $d00a
        lda sprite6xtab,x
        sta $d00c
        lda sprite7xtab,x
        sta $d00e
        lda sprite8xtab,x
        sta $d010

        lda #223
        ldy #scrollsprites1/$40
        .for (var i = 1; i < 8; i++)
        {
            sta $d001 + i*2
            sty screen + $3f8 + i
            .if (i!=7){iny}
        }
        sta $d001
        ldy #coverscrollsprite/$40
        sty screen + $3f8

        jsr muzak.play

        ldx scrollpos
        dex
        bpl !+
        jsr move_scrolltext_buffer
        jsr copy_graphics_sprite1
        ldx #7
!:      stx scrollpos


endirq:
        lda #<irq2
        sta $fffe
        lda #>irq2
        sta $ffff
        lda #irq2_line
        sta $d012

        inc $d019
        pla
        tay
        pla
        tax
        pla
        rti
}


        
.var sprites_pos_x_list = List().add(
    176,200,224,248,272,296,320
)
sprite1xtab:
.fill 8, sprites_pos_x_list.get(0) +i
sprite2xtab:
.fill 8, sprites_pos_x_list.get(1) +i
sprite3xtab:
.fill 8, sprites_pos_x_list.get(2) +i
sprite4xtab:
.fill 8, sprites_pos_x_list.get(3) +i
sprite5xtab:
.fill 8, sprites_pos_x_list.get(4) +i
sprite6xtab:
.fill 8, sprites_pos_x_list.get(5) +i
sprite7xtab:
.fill 8, sprites_pos_x_list.get(6) +i
sprite8xtab:
.byte $e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0


* = $1000 "sid"
.fill muzak.size, muzak.getData(i)
* = * "GRAPHICS VIRTUAL"
.var kla = LoadBinary("gfx/bitmap.koa",BF_KOALA)
.var spr = LoadPicture("gfx/sprites_only.bmp",
    List().add($6c5eb5, $000000, $352879, $444444))
.var sp2 = LoadPicture("gfx/sprites_water.bmp",
    List().add($68372b, $000000, $70a4b2, $000000))
.var chr = LoadPicture("gfx/charset.png")
.align $100
copy_colorram:
    .fill kla.getColorRamSize(), kla.getColorRam(i)
copy_screenram:
    .fill kla.getScreenRamSize(), kla.getScreenRam(i)
.align $100
charset:
    .for (var y = 0; y < 16; y++)
    {
        .fill 64, chr.getSinglecolorByte(i,y)
    }

* = $4000 "VIC: bitmap"
bitmap:
    .fill kla.getBitmapSize(), kla.getBitmap(i)
* = $6000 "VIC: screen"
real_screenram:
screen:
    .fill $400, $aa
* = $6400 "VIC: sprites"
bitmapsprites:
    .for (var x = 0; x < 15; x++)
    {
        .for (var y = 0; y < 21; y++)
        {
            .byte spr.getMulticolorByte(x*3+0, y)
            .byte spr.getMulticolorByte(x*3+1, y)
            .byte spr.getMulticolorByte(x*3+2, y)
        }
        .byte $00
    }
    .for (var x = 0; x < 1; x++)
    {
        .for (var y = 0; y < 21; y++)
        {
            .byte sp2.getMulticolorByte(x*3+0, y)
            .byte sp2.getMulticolorByte(x*3+1, y)
            .byte sp2.getMulticolorByte(x*3+2, y)
        }
        .byte $00
    }
    .for (var x = 15; x < 22; x++)
    {
        .for (var y = 0; y < 21; y++)
        {
            .byte spr.getMulticolorByte(x*3+0, y)
            .byte spr.getMulticolorByte(x*3+1, y)
            .byte spr.getMulticolorByte(x*3+2, y)
        }
        .byte $00
    }
    .for (var x = 1; x < 2; x++)
    {
        .for (var y = 0; y < 21; y++)
        {
            .byte sp2.getMulticolorByte(x*3+0, y)
            .byte sp2.getMulticolorByte(x*3+1, y)
            .byte sp2.getMulticolorByte(x*3+2, y)
        }
        .byte $00
    }
coverscrollsprite:
    .import binary "gfx/coversrpite.sprite",0,64
samarlogo:
    .import binary "gfx/samar_flashback_sprites.bin",0,64*4
    .fill 64,$00
flashbacklogo:
    .import binary "gfx/samar_flashback_sprites.bin",64*4
scrollsprites1:
* = * "sprites for scroll"
.fill 6*64, $00

* = * "rest"
.align $100
.encoding "screencode_upper"
text:
.text "     ''IF A TECHNOLOGICAL FEAT IS POSSIBLE, MAN WILL DO IT. ALMOST AS IF IT'S WIRED"
.text " INTO THE CORE OF OUR BEING'' "
.text "THAT'S WHY WE ASSEMBLED ''GHOST IN THE SYDNEY''... A SMALL CONTRIBUTION TO THE FLASHBACK 2019 DEMO PARTY "
.text "PRODUCED BY GOLARA ISILDUR AND JAMMER OF SAMAR PRODUCTIONS. "
.text "GREETINGS TO THE WHOLE DEMOSCENE FAMILY AND EVERYONE AT THE PARTY.  "
.text "IF YOU FIND SOME BUGS HERE AND THERE, PLEASE EXCUSE US, THIS INTRO WAS MADE IN LESS THAN 24 HOURS"
.text "      END OF TRANSMITION          @"

//y= 12
//x= 3

//anima03.kla is a 160x200 koala file with the
//8 frames of animation for the rays on the tits
//gotta "cut" it out from the image and store it in
//such a way that it is easier to copy over

//With some more carefull planing this could be reduced quite a bit
//maybe even make it work with just copying colorram and screenram
//but it was a quick job, there was enough rastertime and ram... oh well

.var anim = LoadBinary("gfx/anima03.kla", BF_KOALA)
.var anim_bitmap_sep_list = List().add(List(),List(),List(),List(),List(),List(),List(),List())
.var anim_screen_sep_list = List().add(List(),List(),List(),List(),List(),List(),List(),List())
.var anim_colram_sep_list = List().add(List(),List(),List(),List(),List(),List(),List(),List())

.var aposx = List().add(0, 19, 0, 19, 0, 19, 0, 19)
.var aposy = List().add(0, 0, 5, 5, 10, 10, 15, 15)
.var maxx = List().add(3,3,2,0,1)
    .for (var a1 = 0; a1 < 8; a1++)
    {
        .for (var y = 0; y < 5; y++)
        {
            .for (var x = 0; x < 19; x++)
            {
                .var idx = (aposx.get(a1) + x)*8 + (aposy.get(a1)+y)*320
                .for (var i = 0; i < 8; i++)
                {
                    .eval anim_bitmap_sep_list.get(a1).add(anim.getBitmap(idx+i))
                }

                .eval idx = (aposx.get(a1)+x) + (aposy.get(a1)+y)*40
                .eval anim_screen_sep_list.get(a1).add(anim.getScreenRam(idx))
                .eval anim_colram_sep_list.get(a1).add(anim.getColorRam(idx))
            }
        }
    }

.align $100
* = * "anim bitmap"
anim_bitmap:
.for (var b = 0; b < anim_bitmap_sep_list.get(0).size(); b++)
{
    .byte anim_bitmap_sep_list.get(7).get(b)
    .byte anim_bitmap_sep_list.get(6).get(b)
    .byte anim_bitmap_sep_list.get(5).get(b)
    .byte anim_bitmap_sep_list.get(4).get(b)
    .byte anim_bitmap_sep_list.get(3).get(b)
    .byte anim_bitmap_sep_list.get(2).get(b)
    .byte anim_bitmap_sep_list.get(1).get(b)
    .byte anim_bitmap_sep_list.get(0).get(b)
}
* = * "anim screen"
anim_screen:
.for (var b = 0; b < anim_screen_sep_list.get(0).size(); b++)
{
    .byte anim_screen_sep_list.get(7).get(b)
    .byte anim_screen_sep_list.get(6).get(b)
    .byte anim_screen_sep_list.get(5).get(b)
    .byte anim_screen_sep_list.get(4).get(b)
    .byte anim_screen_sep_list.get(3).get(b)
    .byte anim_screen_sep_list.get(2).get(b)
    .byte anim_screen_sep_list.get(1).get(b)
    .byte anim_screen_sep_list.get(0).get(b)
}
* = * "anim colram"
anim_colram:
.for (var b = 0; b < anim_colram_sep_list.get(0).size(); b++)
{
    .byte anim_colram_sep_list.get(7).get(b)
    .byte anim_colram_sep_list.get(6).get(b)
    .byte anim_colram_sep_list.get(5).get(b)
    .byte anim_colram_sep_list.get(4).get(b)
    .byte anim_colram_sep_list.get(3).get(b)
    .byte anim_colram_sep_list.get(2).get(b)
    .byte anim_colram_sep_list.get(1).get(b)
    .byte anim_colram_sep_list.get(0).get(b)
}

//graphics are stored in such a way that this one function
//can copy any frame (0-7) with just a number of a frame in X reg.
copy_bitmap_animation:
{
        lda skipper:#$00
        inc skipper
        and #3
        beq !+
        rts
!:
        ldx idx:#$07
        .for (var y = 0; y < 5; y++)
        {
            .for (var xx = 0; xx < 19-maxx.get(y); xx++)
            {
                
                .for (var i = 0; i < 8; i++)
                {
                    lda anim_bitmap + (y*19*8 + xx*8 + i)*8, x
                    sta bitmap + (12+y)*320 + (xx+3)*8 +i
                }
                
                lda anim_screen + (y*19 + xx)*8, x
                sta screen + (12+y)*40 + (xx+3)

                lda anim_colram + (y*19 +xx)*8, x
                sta $d800  + (12+y)*40 + (xx+3) 
            }
        }
        dex
        bpl !+
        ldx #$07
!:      stx idx
        rts
}

copy_graphics_sprite1:
        .for (var i = 0; i < 7; i++)
        {
            ldy #00
            ldx scrollbuffer + 00 + i*3
            jsr draw_list.get(i)
            iny
            ldx scrollbuffer + 01 + i*3
            jsr draw_list.get(i)
            iny
            ldx scrollbuffer + 02 + i*3
            jsr draw_list.get(i)
        }
        rts

//this function does a colorfade of a multicolor bitmap
//will also work on highres, you just have to remove the
//code for colorram. 

//fade_data is a 1000 bytes big table (40x25, byte for each char)
//containing a number from 6 to ff. Each byte is decreased. If the byte is 0
//then it is done and skipped. If byte is biger than 6, then it's still black
//(or on this case pink, as that's the background color we start with)
//if it's 1-5 that is an index to a colortable. 
do_fade_in:
            ldy #$00
loop:
            ldx fadesrc1:fade_data, y
            change_to_bitzp:beq next_byte
            change_to_inx:dex
            txa
            sta fadesrc2:fade_data, y
            cmp #$05
            bcs next_byte
            stx zptmp1

            ldx src1:copy_screenram, y
            stx zptmp3
            lda fadeadc_hi, x
            adc zptmp1
            tax
            lda fadegen, x
            asl
            asl
            asl
            asl
            sta zptmp2
            ldx zptmp3
            lda fadeadc_lo, x
            adc zptmp1
            tax
            lda fadegen, x
            ora zptmp2
            sta dst1:real_screenram, y

            ldx src2:copy_colorram, y
            lda fadeadc_lo, x
            adc zptmp1
            tax
            lda fadegen, x
            sta dst2:real_colorram, y

next_byte:
            iny
            bne !+
            inc fadesrc1 + 1
            inc fadesrc2 + 1
            inc src1 + 1
            inc src2 + 1
            inc dst1 + 1
            inc dst2 + 1
!:
            cpy #<(real_screenram + 1000)
            bne loop
            lda dst1 + 1
            cmp #>(real_screenram + 1000)
            bne loop

            lda #>fade_data
            sta fadesrc1 + 1
            sta fadesrc2 + 1
            lda #>copy_screenram
            sta src1 + 1
            lda #>copy_colorram
            sta src2 + 1
            lda #>real_colorram
            sta dst2 + 1
            lda #>real_screenram
            sta dst1 + 1
            rts

.align $100
intro_sintab:
.byte 5,5,5,6,7,8,9,11,13,15,18,21,24,27,30,34
.byte 38,42,46,50,54,58,63,67,71,76,80,85,89,93,97,101
.byte 105,108,112,115,118,121,123,126,128,130,131,133,134,134,135,135
.byte 135,135,134,133,132,130,129,127,125,122,120,117,114,111,108,104
.byte 101,97,94,90,86,83,79,75,72,68,65,61,58,55,52,50
.byte 47,45,43,41,39,38,37,36,36,36,36,36,37,38,39,41
.byte 43,45,47,50,53,56,59,63,67,71,75,80,84,89,94,98
.byte 103,108,113,118,123,128,133,138,143,147,152,156,160,164,168,172
.byte 175,178,181,184,186,188,190,191,193,194,194,195,195,194,194,193
.byte 192,191,189,187,185,183,181,178,175,172,169,166,163,159,156,152
.byte 149,145,142,138,135,132,129,125,122,120,117,115,112,110,108,107
.byte 105,104,104,103,103,103,103,104,104,106,107,109,111,113,115,118
.byte 121,124,128,131,135,139,143,148,152,157,161,166,170,175,180,185
.byte 189,194,198,203,207,211,215,219,222,226,229,232,235,237,240,241
.byte 243,245,246,246,246,246
.fill 26, 246


.var logolist = List().add(
  367,368,368,369,369,369,369,369,368,367,366,365,364,363,361,359
, 357,355,353,350,348,345,342,339,336,333,329,326,322,318,315,311
, 307,303,299,295,291,286,282,278,274,270,266,261,257,253,249,245
, 242,238,234,230,227,223,220,217,214,211,208,205,203,200,198,196
, 194,192,190,189,187,186,185,184,183,183,182,182,182,182,182,182
, 183,183,184,185,186,187,188,189,191,192,194,195,197,199,201,203
, 205,207,209,211,213,215,217,219,221,223,225,227,229,231,233,235
, 236,238,240,241,243,244,245,246,247,248,249,250,251,251,252,252
, 252,252,252,252,251,251,250,250,249,248,247,246,245,244,242,241
, 239,238,236,234,232,231,229,227,225,223,221,219,217,215,213,211
, 209,207,205,203,201,199,198,196,195,193,192,191,189,188,187,187
, 186,185,185,185,185,185,185,185,185,186,187,188,189,190,191,193
, 195,197,199,201,203,205,208,211,214,216,220,223,226,230,233,237
, 240,244,248,252,256,260,264,268,272,276,280,284,289,293,297,301
, 305,309,313,316,320,324,327,331,334,337,340,343,346,349,351,354
, 356,358,360,362,363,365,366,367,368,368,369,369,369,369,369,368

)
logostab:
.fill $100, logolist.get(i)
logostabd010:
.function getd010(o)
{
    .var b = 0
    .for (var i = 0; i < 5; i++)
    {
        .if (logolist.get(o) + i*24 >= 256)
        {
            .eval b = b | (1<<i)
        }
    }
    .return b
}
.for (var o = 0; o < $100; o++)
{
    .byte getd010(o)
}


//these can be easely generated on the fly if you care about size
.align $100
* = * "tables"
fadeadc_hi:
.for (var l = 0; l < 16; l++)
{
    .fill 16, l*6
}
fadeadc_lo:
.for (var i = 0; i < 16; i++)
{
    .fill 16, i*6
}
fade_data:
.for (var y = 0; y < 5; y++)
{
    .fill 40*5, 5+(5*y)
}

fadegen:
.byte $0,$9,$2,$4,$a,$a
.byte $1,$7,$f,$a,$a,$a
.byte $2,$4,$a,$a,$a,$a
.byte $3,$c,$a,$a,$a,$a
.byte $4,$a,$a,$a,$a,$a
.byte $5,$c,$a,$a,$a,$a
.byte $6,$b,$4,$a,$a,$a
.byte $7,$f,$a,$a,$a,$a
.byte $8,$a,$a,$a,$a,$a
.byte $9,$2,$4,$a,$a,$a
.byte $a,$a,$a,$a,$a,$a
.byte $b,$4,$4,$4,$4,$a
.byte $c,$a,$a,$a,$a,$a
.byte $d,$f,$a,$a,$a,$a
.byte $e,$c,$a,$a,$a,$a
.byte $f,$a,$a,$a,$a,$a
//bottom sprites use this fade table, because there's to few color steps
//in the table above for the specific colors used by these sprites...
//it just looked better.
fadegen2:
.byte $0,$9,$2,$4,$a,$2
.byte $1,$7,$f,$a,$a,$2
.byte $2,$4,$a,$a,$a,$2
.byte $3,$c,$a,$a,$a,$2
.byte $4,$a,$a,$a,$a,$2
.byte $5,$c,$a,$a,$a,$2
.byte $6,$b,$4,$a,$a,$2
.byte $7,$f,$a,$a,$a,$2
.byte $8,$a,$a,$a,$a,$2
.byte $9,$2,$4,$a,$a,$2
.byte $a,$a,$a,$a,$a,$2
.byte $b,$4,$4,$4,$4,$2
.byte $c,$a,$a,$a,$a,$2
.byte $d,$f,$a,$a,$a,$2
.byte $e,$c,$a,$a,$a,$2
.byte $f,$a,$a,$a,$a,$2
